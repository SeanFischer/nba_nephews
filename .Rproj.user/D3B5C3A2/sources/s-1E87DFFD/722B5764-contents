mvp_analysis <- function(voting_returns) {
  require(tabulizer)
  require(dplyr)
  require(tidyr)
  tab1 <- extract_tables(voting_returns, output = "data.frame")
  tab1 <- bind_rows(tab1)
  tab1 <- tab1 %>% distinct()
  tab1 <- tab1 %>%
    mutate(Voter = tab1[, 1])
  tab1 <- gather(
    tab1, 
    slot, 
    player, 
    X1st.Place..10.points.:X5th.Place..1.point.,
    factor_key = TRUE) %>%
    arrange(Voter) %>%
    mutate(points = case_when(slot == "X1st.Place..10.points." ~ 10,
                              slot == "X2nd.Place..7.points." ~ 7,
                              slot == "X3rd.Place..5.points." ~ 5,
                              slot == "X4th.Place..3.points." ~ 3,
                              slot == "X5th.Place..1.point." ~ 1))
  tab2 <- tab1 %>%
    group_by(player) %>%
    summarise(total_points = sum(points),
              avg_points = total_points / 100)
  
  tab1 <- tab1 %>%
    inner_join(tab2 %>% select(-total_points)) %>%
    mutate(difference = abs(points - avg_points),
           sq_difference = difference ^ 2)
  
  tab3 <- tab1 %>%
    group_by(Voter) %>%
    summarise(total_diff = sum(sq_difference),
              avg_diff = mean(sq_difference),
              sqrt_diff = sqrt(avg_diff)) %>%
    ungroup() %>%
    mutate(z_score = (total_diff - mean(total_diff)) / sd(total_diff))
  out <- list(tab1, tab2, tab3)
  names(out) <- c("raw", "players", "writers")
  return(out)
}

roy_mip_6man_coach_dpoy_analysis <- function(voting_returns) {
  require(tabulizer)
  require(dplyr)
  require(tidyr)
  tab1 <- extract_tables(voting_returns, output = "data.frame")
  tab1 <- bind_rows(tab1)
  tab1 <- tab1 %>% distinct()
  tab1 <- tab1 %>%
    mutate(Voter = tab1[, 1])
  tab1 <- gather(
    tab1, 
    slot, 
    player, 
    X1st.Place..5.points.:X3rd.Place..1.point.,
    factor_key = TRUE) %>%
    arrange(Voter) %>%
    mutate(points = case_when(slot == "X1st.Place..5.points." ~ 5,
                              slot == "X2nd.Place..3.points." ~ 3,
                              slot == "X3rd.Place..1.point." ~ 1))
  tab2 <- tab1 %>%
    group_by(player) %>%
    summarise(total_points = sum(points),
              avg_points = total_points / 100)
  
  tab1 <- tab1 %>%
    inner_join(tab2 %>% select(-total_points)) %>%
    mutate(difference = abs(points - avg_points),
           sq_difference = difference ^ 2)
  
  tab3 <- tab1 %>%
    group_by(Voter) %>%
    summarise(total_diff = sum(sq_difference),
              avg_diff = mean(sq_difference),
              sqrt_diff = sqrt(avg_diff)) %>%
    ungroup() %>%
    mutate(z_score = (total_diff - mean(total_diff)) / sd(total_diff))
  out <- list(tab1, tab2, tab3)
  names(out) <- c("raw", "players", "writers")
  return(out)
}

all_nba_analysis <- function(voting_returns) {
  require(tabulizer)
  require(dplyr)
  require(tidyr)
  require(stringr)
  tab1 <- extract_tables(voting_returns, output = "data.frame")
  tab1 <- bind_rows(tab1)
  tab1 <- tab1[, c(2, 3, 5:9, 11:15, 17:21)]
  colnames(tab1) <- c("Voter", "Affiliation", "1st Team Forward_1",
                      "1st Team Forward_2", "1st Team Center", 
                      "1st Team Guard_1", "1st Team Guard_2",
                      "2nd Team Forward_1", "2nd Team Forward_2",
                      "2nd Team Center", "2nd Team Guard_1", "2nd Team Guard_2",
                      "3rd Team Forward_1", "3rd Team Forward_2",
                      "3rd Team Center", "3rd Team Guard_1", "3rd Team Guard_2")
  tab1 <- tab1[-1, ]
  tab1 <- gather(
    tab1, 
    slot, 
    player, 
    `1st Team Forward_1`:`3rd Team Guard_2`,
    factor_key = TRUE
  ) %>%
    arrange(Voter) %>%
    mutate(points = case_when(str_detect(slot, "1st Team") ~ 5,
                              str_detect(slot, "2nd Team") ~ 3,
                              str_detect(slot, "3rd Team") ~ 1))
  tab2 <- tab1 %>%
    group_by(player) %>%
    summarise(total_points = sum(points),
              avg_points = total_points / 100)
  
  tab1 <- tab1 %>%
    inner_join(tab2 %>% select(-total_points)) %>%
    mutate(difference = abs(points - avg_points),
           sq_difference = difference ^ 2)
  
  tab3 <- tab1 %>%
    group_by(Voter) %>%
    summarise(total_diff = sum(sq_difference),
              avg_diff = mean(sq_difference),
              sqrt_diff = sqrt(avg_diff)) %>%
    ungroup() %>%
    mutate(z_score = (total_diff - mean(total_diff)) / sd(total_diff))
  out <- list(tab1, tab2, tab3)
  names(out) <- c("raw", "players", "writers")
  return(out)
}

all_def_analysis <- function(voting_returns) {
  require(tabulizer)
  require(dplyr)
  require(tidyr)
  require(stringr)
  tab1 <- extract_tables(voting_returns, output = "data.frame")
  tab1 <- bind_rows(tab1)
  tab1 <- tab1[, c(2, 3, 5:9, 11:15)]
  colnames(tab1) <- c("Voter", "Affiliation", "1st Team Forward_1",
                      "1st Team Forward_2", "1st Team Center", 
                      "1st Team Guard_1", "1st Team Guard_2",
                      "2nd Team Forward_1", "2nd Team Forward_2",
                      "2nd Team Center", "2nd Team Guard_1", "2nd Team Guard_2")
  tab1 <- tab1 %>% distinct()
  tab1 <- tab1[-1, ]
  tab1 <- gather(
    tab1, 
    slot, 
    player, 
    `1st Team Forward_1`:`2nd Team Guard_2`,
    factor_key = TRUE
  ) %>%
    arrange(Voter) %>%
    mutate(points = case_when(str_detect(slot, "1st Team") ~ 2,
                              str_detect(slot, "2nd Team") ~ 1))
  tab2 <- tab1 %>%
    group_by(player) %>%
    summarise(total_points = sum(points),
              avg_points = total_points / 100)
  
  tab1 <- tab1 %>%
    inner_join(tab2 %>% select(-total_points)) %>%
    mutate(difference = abs(points - avg_points),
           sq_difference = difference ^ 2)
  
  tab3 <- tab1 %>%
    group_by(Voter) %>%
    summarise(total_diff = sum(sq_difference),
              avg_diff = mean(sq_difference),
              sqrt_diff = sqrt(avg_diff)) %>%
    ungroup() %>%
    mutate(z_score = (total_diff - mean(total_diff)) / sd(total_diff))
  out <- list(tab1, tab2, tab3)
  names(out) <- c("raw", "players", "writers")
  return(out)
}

all_rook_analysis <- function(voting_returns) {
  require(tabulizer)
  require(dplyr)
  require(tidyr)
  require(stringr)
  tab1 <- extract_tables(voting_returns, output = "data.frame")
  tab1 <- bind_rows(tab1)
  tab1 <- tab1[, c(2, 3, 5:9, 11:15)]
  colnames(tab1) <- c("Voter", "Affiliation", "1st Team_1",
                      "1st Team_2", "1st Team_3", 
                      "1st Team_4", "1st Team_5",
                      "2nd Team_1", "2nd Team_2",
                      "2nd Team_3", "2nd Team_4", "2nd Team_5")
  tab1 <- tab1 %>% distinct()
  tab1 <- tab1[-1, ]
  tab1 <- gather(
    tab1, 
    slot, 
    player, 
    `1st Team_1`:`2nd Team_5`,
    factor_key = TRUE
  ) %>%
    arrange(Voter) %>%
    mutate(points = case_when(str_detect(slot, "1st Team") ~ 2,
                              str_detect(slot, "2nd Team") ~ 1))
  tab2 <- tab1 %>%
    group_by(player) %>%
    summarise(total_points = sum(points),
              avg_points = total_points / 100)
  
  tab1 <- tab1 %>%
    inner_join(tab2 %>% select(-total_points)) %>%
    mutate(difference = abs(points - avg_points),
           sq_difference = difference ^ 2)
  
  tab3 <- tab1 %>%
    group_by(Voter) %>%
    summarise(total_diff = sum(sq_difference),
              avg_diff = mean(sq_difference),
              sqrt_diff = sqrt(avg_diff)) %>%
    ungroup() %>%
    mutate(z_score = (total_diff - mean(total_diff)) / sd(total_diff))
  out <- list(tab1, tab2, tab3)
  names(out) <- c("raw", "players", "writers")
  return(out)
}
