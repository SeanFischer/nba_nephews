library(dplyr)
library(tidyr)

source("SCRIPTS/analysis_functions.R")

mvp_15_16 <- mvp_analysis("DATA/15_16/15_16_MVP.csv",
                          year = "2015-2016")
roy_15_16 <- roy_mip_6man_coach_dpoy_analysis("DATA/15_16/15_16_ROY.csv",
                                              year = "2015-2016",
                                              category = "ROY")
mip_15_16 <- roy_mip_6man_coach_dpoy_analysis("DATA/15_16/15_16_MIP.csv",
                                              year = "2015-2016",
                                              category = "MIP")
sixth_man_15_16 <- roy_mip_6man_coach_dpoy_analysis("DATA/15_16/15_16_6MAN.csv",
                                                    year = "2015-2016",
                                                    category = "6MAN")
coach_15_16 <- roy_mip_6man_coach_dpoy_analysis("DATA/15_16/15_16_COACH.csv",
                                                year = "2015-2016",
                                                category = "COACH")
dpoy_15_16 <- roy_mip_6man_coach_dpoy_analysis("DATA/15_16/15_16_DPOY.csv",
                                               year = "2015-2016",
                                               category = "DPOY")
all_nba_15_16 <- all_nba_analysis("DATA/15_16/15_16_ALL_NBA_FIRST.csv",
                                  "DATA/15_16/15_16_ALL_NBA_SECOND.csv",
                                  "DATA/15_16/15_16_ALL_NBA_THIRD.csv",
                                  year = "2015-2016")
all_def_15_16 <- all_def_analysis("DATA/15_16/15_16_ALL_DEF_FIRST.csv",
                                  "DATA/15_16/15_16_ALL_DEF_SECOND.csv",
                                  year = "2015-2016")
all_rook_15_16 <- all_rook_analysis("DATA/15_16/15_16_ALL_ROOK_FIRST.csv",
                                    "DATA/15_16/15_16_ALL_ROOK_SECOND.csv",
                                    year = "2015-2016")

outlets_15_16 <- mvp_15_16$raw %>%
  bind_rows(
    roy_15_16$raw, 
    mip_15_16$raw,
    sixth_man_15_16$raw,
    coach_15_16$raw,
    dpoy_15_16$raw,
    all_nba_15_16$raw,
    all_def_15_16$raw,
    all_rook_15_16$raw
  ) %>%
  group_by(Affiliation) %>%
  summarise()

writers_15_16_all <- mvp_15_16$writers %>%
  bind_rows(
    roy_15_16$writers, 
    mip_15_16$writers,
    sixth_man_15_16$writers,
    coach_15_16$writers,
    dpoy_15_16$writers,
    all_nba_15_16$writers,
    all_def_15_16$writers,
    all_rook_15_16$writers) %>%
  mutate(category = c(rep("MVP", nrow(mvp_15_16$writers)),
                      rep("ROY", nrow(roy_15_16$writers)),
                      rep("MIP", nrow(mip_15_16$writers)),
                      rep("6MAN", nrow(sixth_man_15_16$writers)),
                      rep("COACH", nrow(coach_15_16$writers)),
                      rep("DPOY", nrow(dpoy_15_16$writers)),
                      rep("ALL_NBA", nrow(all_nba_15_16$writers)),
                      rep("ALL_DEF", nrow(all_def_15_16$writers)),
                      rep("ALL_ROOK", nrow(all_rook_15_16$writers))))

writers_15_16_total <- writers_15_16_all %>%
  group_by(Voter, Affiliation) %>%
  summarise(n_ballots = n(),
            total_sqrt_diff = sum(sqrt_diff),
            avg_total_sqrt_diff = total_sqrt_diff / n_ballots,
            sd_total_sqrt_diff = sd(sqrt_diff))

raw_votes_15_16 <- mvp_15_16$raw %>%
  bind_rows(
    roy_15_16$raw, 
    mip_15_16$raw,
    sixth_man_15_16$raw,
    coach_15_16$raw,
    dpoy_15_16$raw,
    all_nba_15_16$raw,
    all_def_15_16$raw,
    all_rook_15_16$raw
  ) %>%
  arrange(Voter)